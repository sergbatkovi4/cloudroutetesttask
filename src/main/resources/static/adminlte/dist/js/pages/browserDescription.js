/**
 * Created by serhii on 4/27/16.
 */
$(function () {

    var showError = function (response, idContainer) {
            var error = '<div class="alert alert-danger">' +
                '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                '<strong>Error!</strong> Message:   ' + response.responseText + '</div>';

            $(idContainer).html(error);
        },

        table = $("#browserDescriptionTable").DataTable({
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $(nRow).attr('data-id', aData.id);
            },
            "rowId": "id",
            "ajax": {
                "url": "/browser/descriptions",
                "type": "GET",
                "dataSrc": "",
                "error": function (response) {
                    showError(response, "#error-container");
                }
            },

            "columns": [
                {"data": "renderingEngine"},
                {"data": "name"},
                {"data": "platform"},
                {"data": "engineVersion"},
                {"data": "cssGrade"}
            ],


        });

    $('#browserDescriptionTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }

    });

    $("#delete").click(function () {
        if ($('table .selected').data("id") === "0" || $('table .selected').data("id")) {
            $.ajax({
                type: "DELETE",
                url: "/browser/descriptions/" + $('table .selected').data("id"),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    table.row('.selected').remove().draw(false);
                },
                "error": function (response) {
                    showError(response, "#error-container");
                }

            });
        }
    });

    $("#add").click(function () {
        $(".modal-title").text("Addition Browser Description");
        $("#put").hide();
        $("#save").show();

    });

    $("#update").click(function (event) {
        if ($('table .selected').data("id") === "0" || $('table .selected').data("id")) {
            $("#modalBrowserDescription").modal('toggle');
            $(".modal-title").text("Update Browser Description");
            $("#save").hide();
            $("#put").show();
            var editedItem = table.row('.selected').data();
            setDataForm(editedItem);
        }
    });

    function setDataForm(item) {
        $("#renderingEngine").val(item.renderingEngine);
        $("#name").val(item.name);
        $("#platform").val(item.platform);
        $("#engineVersion").val(item.engineVersion);
        $("#cssGrade").val(item.cssGrade);
    }

    function fillData(data) {
        data.renderingEngine = $("#renderingEngine").val(),
            data.name = $("#name").val(),
            data.platform = $("#platform").val(),
            data.engineVersion = $("#engineVersion").val(),
            data.cssGrade = $("#cssGrade").val();
    }

    $("#put").click(function () {
        var data = {},
            editedItem = table.row('.selected').data();
        fillData(data);
        data.id = editedItem.id;
        $.ajax({
            type: "PUT",
            url: "/browser/descriptions/",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (updatedItem) {
                table.row('.selected').remove().draw(false);
                table.row.add(updatedItem).draw();
                $('#modalBrowserDescription').modal('toggle');
            },
            "error": function (response) {
                showError(response, "#error-modal-container");
            }
        });
    });

    $("#save").click(function () {
        var data = {};
        fillData(data);
        $.ajax({
            type: "POST",
            url: "/browser/descriptions",
            data: JSON.stringify(data),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (savedItem) {
                table.row.add(savedItem).draw();
                $('#modalBrowserDescription').modal('toggle');
            },
            "error": function (response) {
                showError(response, "#error-modal-container");
            }
        });
    });

    $(".modal").on("hidden.bs.modal", function () {
        $("input").val("");
        $("#error-modal-container").html("");
    });
});