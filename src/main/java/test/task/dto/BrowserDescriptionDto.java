package test.task.dto;

/**
 * Created by serhii on 4/27/16.
 */
public class BrowserDescriptionDto {
    private Long id;
    private String renderingEngine;
    private String name;
    private String platform;
    private String engineVersion;
    private String cssGrade;

    public BrowserDescriptionDto() {
    }

    public BrowserDescriptionDto(String renderingEngine, String name,
                                 String platform, String engineVersion, String cssGrade) {
        this.renderingEngine = renderingEngine;
        this.name = name;
        this.platform = platform;
        this.engineVersion = engineVersion;
        this.cssGrade = cssGrade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRenderingEngine() {
        return renderingEngine;
    }

    public void setRenderingEngine(String renderingEngine) {
        this.renderingEngine = renderingEngine;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getEngineVersion() {
        return engineVersion;
    }

    public void setEngineVersion(String engineVersion) {
        this.engineVersion = engineVersion;
    }

    public String getCssGrade() {
        return cssGrade;
    }

    public void setCssGrade(String cssGrade) {
        this.cssGrade = cssGrade;
    }

    @Override
    public String toString() {
        return "BrowserDescriptionDto{" +
                "id=" + id +
                ", renderingEngine='" + renderingEngine + '\'' +
                ", name='" + name + '\'' +
                ", platform='" + platform + '\'' +
                ", engineVersion='" + engineVersion + '\'' +
                ", cssGrade='" + cssGrade + '\'' +
                '}';
    }
}
