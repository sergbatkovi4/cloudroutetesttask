package test.task.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by serhii on 4/28/16.
 */
@Entity
public class BrowserDescription {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;
    @Column(name = "renderingEngine")
    private String renderingEngine;
    @Column(name = "name")
    private String name;
    @Column(name = "platform")
    private String platform;
    @Column(name = "engineVersion")
    private String engineVersion;
    @Column(name = "cssGrade")
    private String cssGrade;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRenderingEngine() {
        return renderingEngine;
    }

    public void setRenderingEngine(String renderingEngine) {
        this.renderingEngine = renderingEngine;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getEngineVersion() {
        return engineVersion;
    }

    public void setEngineVersion(String engineVersion) {
        this.engineVersion = engineVersion;
    }

    public String getCssGrade() {
        return cssGrade;
    }

    public void setCssGrade(String cssGrade) {
        this.cssGrade = cssGrade;
    }

    @Override
    public String toString() {
        return "BrowserDescription{" +
                "id=" + id +
                ", renderingEngine='" + renderingEngine + '\'' +
                ", name='" + name + '\'' +
                ", platform='" + platform + '\'' +
                ", engineVersion='" + engineVersion + '\'' +
                ", cssGrade='" + cssGrade + '\'' +
                '}';
    }
}
