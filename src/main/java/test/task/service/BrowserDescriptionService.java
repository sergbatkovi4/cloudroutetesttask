package test.task.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.task.domain.BrowserDescription;
import test.task.dto.BrowserDescriptionDto;
import test.task.repository.BrowserDescriptionRepository;
import test.task.util.MappingDtoUtil;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by serhii on 4/27/16.
 */
@Service
public class BrowserDescriptionService {

    @Autowired
    private BrowserDescriptionRepository browserDescriptionRepository;
    private Logger LOGGER = Logger.getLogger(BrowserDescriptionService.class);

    public List<BrowserDescriptionDto> getList() {
        LOGGER.info("Getting browser description");
        List<BrowserDescriptionDto> browserDescriptionDtoLocalList = new ArrayList<BrowserDescriptionDto>();
        browserDescriptionRepository.findAll().forEach((browserDescription) ->
                browserDescriptionDtoLocalList.add(MappingDtoUtil.mappingBrowserDescriptionDto(browserDescription)));
        LOGGER.debug(String.format("Got browser description: %s", browserDescriptionDtoLocalList));
        return browserDescriptionDtoLocalList;
    }

    public BrowserDescriptionDto save(BrowserDescriptionDto browserDescriptionDto) {
        if (browserDescriptionDto.getId() != null) {
            LOGGER.info(String.format("Updating browser description with name: %s", browserDescriptionDto.getName()));
            LOGGER.debug(String.format("Updating browser description: %s", browserDescriptionDto));
        } else {
            LOGGER.info(String.format("Saving browser description with name: %s", browserDescriptionDto.getName()));
            LOGGER.debug(String.format("Saving browser description: %s", browserDescriptionDto));
        }

        BrowserDescription browserDescription = browserDescriptionRepository.save(
                MappingDtoUtil.mappingBrowserDescriptionDomain(browserDescriptionDto));
        if (browserDescriptionDto.getId() != null) {
            LOGGER.info(String.format("Updated browser description with id: %s", browserDescription.getId()));
        } else {
            LOGGER.info(String.format("Saved browser description with id: %s", browserDescription.getId()));
        }
        return MappingDtoUtil.mappingBrowserDescriptionDto(browserDescription);
    }

    public BrowserDescriptionDto delete(Long id) {
        LOGGER.info(String.format("Deleting browser description with name: %s", id));
        BrowserDescription browserDescription = browserDescriptionRepository.findOne(id);
        browserDescriptionRepository.delete(browserDescription);
        LOGGER.info(String.format("Deleted browser description with name: %s", id));
        return MappingDtoUtil.mappingBrowserDescriptionDto(browserDescription);
    }

    public Map<String, Long> getRenderingEngineUsage() {
        LOGGER.info("Getting rendering engine usage list");
        List<Map<String, Long>> hashMapList = browserDescriptionRepository.getRenderingEngineUsage();
        Map<String, Long> result = new HashMap<String, Long>();
        for (Object map : hashMapList) {
            Object[] arrayResult = (Object[]) map;
            result.put((String) arrayResult[0], (Long) arrayResult[1]);
        }
        LOGGER.debug(String.format("Got rendering engine usage list: %s", result));
        LOGGER.info("Got rendering engine usage list");
        return result;
    }
}