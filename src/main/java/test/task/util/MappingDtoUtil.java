package test.task.util;

import test.task.domain.BrowserDescription;
import test.task.dto.BrowserDescriptionDto;

/**
 * Created by serhii on 4/28/16.
 */
public final class MappingDtoUtil {

    public static final BrowserDescriptionDto mappingBrowserDescriptionDto(BrowserDescription browserDescription) {
        BrowserDescriptionDto browserDescriptionDto = new BrowserDescriptionDto();
        if (browserDescription != null) {
            browserDescriptionDto.setId(browserDescription.getId());
            browserDescriptionDto.setName(browserDescription.getName());
            browserDescriptionDto.setRenderingEngine(browserDescription.getRenderingEngine());
            browserDescriptionDto.setPlatform(browserDescription.getPlatform());
            browserDescriptionDto.setEngineVersion(browserDescription.getEngineVersion());
            browserDescriptionDto.setCssGrade(browserDescription.getCssGrade());
        }
        return browserDescriptionDto;
    }

    public static final BrowserDescription mappingBrowserDescriptionDomain(BrowserDescriptionDto browserDescriptionDto) {
        BrowserDescription browserDescription = new BrowserDescription();
        if (browserDescriptionDto != null) {
            browserDescription.setId(browserDescriptionDto.getId());
            browserDescription.setName(browserDescriptionDto.getName());
            browserDescription.setRenderingEngine(browserDescriptionDto.getRenderingEngine());
            browserDescription.setPlatform(browserDescriptionDto.getPlatform());
            browserDescription.setEngineVersion(browserDescriptionDto.getEngineVersion());
            browserDescription.setCssGrade(browserDescriptionDto.getCssGrade());
        }
        return browserDescription;
    }
}

