package test.task.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import test.task.domain.BrowserDescription;

import java.util.List;
import java.util.Map;

/**
 * Created by serhii on 4/28/16.
 */
@Repository
public interface BrowserDescriptionRepository extends CrudRepository<BrowserDescription, Long> {

    List<BrowserDescription> findAll();

    BrowserDescription findOne(Long id);

    <T extends BrowserDescription> T save(T persisted);

    void delete(BrowserDescription deleted);

    @Query("select brDescr.renderingEngine, COUNT(brDescr.renderingEngine) FROM BrowserDescription brDescr group by brDescr.renderingEngine")
    List<Map<String, Long>> getRenderingEngineUsage();
}
