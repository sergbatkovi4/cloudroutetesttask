package test.task.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import test.task.dto.BrowserDescriptionDto;
import test.task.service.BrowserDescriptionService;

import java.util.List;
import java.util.Map;

/**
 * Created by serhii on 4/27/16.
 */
@RestController
@RequestMapping("browser/descriptions")
public class BrowserDescriptionController {
    @Autowired
    private BrowserDescriptionService browserDescriptionService;
    private Logger LOGGER = Logger.getLogger(BrowserDescriptionController.class);

    @RequestMapping(method = RequestMethod.GET)
    public List<BrowserDescriptionDto> list() {
        return browserDescriptionService.getList();

    }

    @RequestMapping(method = RequestMethod.POST)
    public BrowserDescriptionDto add(@RequestBody BrowserDescriptionDto browserDescriptionDto) {
        return browserDescriptionService.save(browserDescriptionDto);

    }

    @RequestMapping(method = RequestMethod.PUT)
    public BrowserDescriptionDto update(@RequestBody BrowserDescriptionDto browserDescriptionDto) {
        return browserDescriptionService.save(browserDescriptionDto);

    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public BrowserDescriptionDto delete(@PathVariable Long id) {
        return browserDescriptionService.delete(id);

    }

    @RequestMapping(value = "usage", method = RequestMethod.GET)
    public Map<String, Long> getRenderingEngineUsage() {
        return browserDescriptionService.getRenderingEngineUsage();

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity handleException(Exception e) {
        LOGGER.error(e);
        String body = e.getMessage();
        if (e.getClass() != null) {
            body = e.getCause().toString();
        }
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(body);
    }

}
