package test.task;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import test.task.domain.BrowserDescription;
import test.task.dto.BrowserDescriptionDto;
import test.task.repository.BrowserDescriptionRepository;
import test.task.util.MappingDtoUtil;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by serhii on 4/28/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebIntegrationTest("server.port:9999")
public class BrowserDescriptionTest {
    @Autowired
    private BrowserDescriptionRepository browserDescriptionRepository;
    @Autowired
    private EmbeddedWebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    private Logger LOGGER = Logger.getLogger(BrowserDescriptionTest.class);

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testBrowserDescriptionRepository() throws Exception {
        BrowserDescription browserDescription = new BrowserDescription();
        browserDescription.setRenderingEngine("Gecko");
        browserDescription.setName("Firefox 1.0");
        browserDescription.setCssGrade("A");
        browserDescription.setEngineVersion("1.7");
        browserDescriptionRepository.save(browserDescription);
        List<BrowserDescription> browserDescriptionList = browserDescriptionRepository.findAll();
        assertEquals("Firefox 1.0", browserDescriptionList.get(0).getName());

    }

    @Test
    public void testBrowserDescriptionList() throws Exception {
        ResultActions result = this.mockMvc.perform(get("/browser/descriptions")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

        String json = result.andReturn().getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        List<BrowserDescriptionDto> browserDescriptionDtoList = mapper.readValue(json, new TypeReference<List<BrowserDescriptionDto>>() {
        });
        assertEquals("Firefox 1.0", browserDescriptionDtoList.get(0).getName());
        LOGGER.info(String.format("Get list result: %s", browserDescriptionDtoList));
    }

    @Test
    public void testBrowserDescriptionDelete() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        BrowserDescriptionDto browserDescriptionDto =
                new BrowserDescriptionDto("Presto", "Opera 8.5Opera 8.5", "Win 95+ / OSX.2+", "-", "A");
        BrowserDescription browserDescription = browserDescriptionRepository.save(
                MappingDtoUtil.mappingBrowserDescriptionDomain(browserDescriptionDto));

        long countBefore = browserDescriptionRepository.count();
        ResultActions result = this.mockMvc.perform(delete("/browser/descriptions/" + browserDescription.getId())
                .contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")));

        String json = result.andReturn().getResponse().getContentAsString();
        BrowserDescriptionDto browserDescriptionDtoResult = mapper.readValue(json, new TypeReference<BrowserDescriptionDto>() {
        });
        long countAfter = browserDescriptionRepository.count();
        assertEquals(browserDescription.getId(), browserDescriptionDtoResult.getId());
        assertEquals(countAfter, countBefore - 1);
        LOGGER.info(String.format("Delete result: %s", browserDescriptionDtoResult));

    }


    @Test
    public void testBrowserDescriptionSave() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        BrowserDescriptionDto browserDescriptionDto =
                new BrowserDescriptionDto("Webkit", "Safari 2.0", "OSX.4+", "419.3", "A");
        String browserDescriptionDtoJson = mapper.writeValueAsString(browserDescriptionDto);


        ResultActions result = this.mockMvc.perform(post("/browser/descriptions").
                contentType(MediaType.parseMediaType("application/json;charset=UTF-8"))
                .content(browserDescriptionDtoJson)
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")));

        String json = result.andReturn().getResponse().getContentAsString();
        BrowserDescriptionDto browserDescriptionDtoResult = mapper.readValue(json, new TypeReference<BrowserDescriptionDto>() {
        });
        assertNotNull(browserDescriptionDtoResult.getId());
        LOGGER.info(String.format("Save result: %s", browserDescriptionDtoResult));

    }

    @Test
    public void testRenderingEngineUsage() throws Exception {
        ResultActions result = this.mockMvc.perform(get("/browser/descriptions/usage")
                .accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));

        String json = result.andReturn().getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Long> mapRenderingEngineUsage = mapper.readValue(json, new TypeReference<Map<String, Long>>() {
        });
        assertNotNull(mapRenderingEngineUsage);
        for (long count : mapRenderingEngineUsage.values()) {
            assertEquals(1, count);
        }

        LOGGER.info(String.format("Get rendering engine usage: %s", mapRenderingEngineUsage));
    }


}
