Requirements for app: 

1. Gradle
2. Java 8
3. Connection to internet.
4. MySQL server

Before launch need set settings for datasource in src/main/resources/application.properties.
Then you can launch app by command: 
 > gradle bootRun