INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Gecko","Mozilla 1.0", "Win 95+/OSX.1+", "1", "A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Gecko", 	"Firefox 1.0", 	"Win 98+ / OSX.2+", "1.7", 	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Gecko", 	"Netscape 7.2", "Win 95+ / Mac OS 8.6-9.2", "1.7", 	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Gecko ",	"Camino 1.5", 	"OSX.3+", 	"1.8",	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Gecko", 	"Seamonkey 1.1", 	"Win 98+ / OSX.2+", "1.8",  "A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("KHTML", 	"Konqureror 3.1", 	"KDE 3.1", 	"3.1",	"C");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("KHTML",	"Konqureror 3.3", 	"KDE 3.3", 	"3.3", 	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Misc", 	"NetFront 3.1", 	"Embedded devices", 	"-", 	"C");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Misc", 	"NetFront 3.4", 	"Embedded devices", 	"-", 	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Misc", 	"Links", 	"Text only", 	"-", 	"X");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Misc", 	"IE Mobile", 	"Windows Mobile 6", 	"-", 	"C");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Presto", 	"Opera 7.0", 	"Win 95+ / OSX.1+", 	"-", 	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Presto", 	"Opera 7.5", 	"Win 95+ / OSX.2+ ",	"-", 	"A");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Tasman", 	"Internet Explorer 5.1",	"Mac OS 7.6-9", 	"1", 	"C");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Tasman", 	"Internet Explorer 5.2", 	"Mac OS 8-X", 	"1", 	"C");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Trident", 	"Internet Explorer 4.0", 	"Win 95+", 	"4", 	"X");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Trident", 	"Internet Explorer 5.0", 	"Win 95+", 	"5", 	"C");
INSERT INTO `browserDescriptionDb`.`browser_description`
(`rendering_engine`, `name`,  `platform`, `engine_version`, `css_grade` ) VALUES ("Webkit", 	"Safari 3.0", 	"OSX.4+", 	"522.1", 	"A");
